# Python - Data Backup & Restore 

## Table of Contents

- [Project Description](#project-description)

- [Technologies Used](#technologies-used)

- [Steps](#steps)

- [Installation](#installation)

- [How to Contribute](#how-to-contribute)

- [Questions](#questions)

- [References](#references)

- [License](#license)


## Project Description

* Wrote a Python script that automates creating backups for EC2 Volumes

* Wrote a Python script that cleans up old EC2 Volume snapshots

* Wrote a Python script that restores EC2 Volumes

## Technologies Used

* Python 

* Boto3

* AWS

## Steps 

Step 1: Create two instance on AWS 

[Instances Running](/images/01_instances_running_on_aws.png)

[Volumes for Instances](/images/02_volumes_for_the_two_ec2_instances.png)

Step 2: Create a python file and import boto library 

[Import boto3](/images/03_create_python_file_and_import_boto_library.png)

Step 3: Create ec2 client 

[EC2 client](/images/04_create_ec2_client.png)

Step 4: Call describe volumes function on ec2 client which will display all volumes 

[Describe Volumes](/images/05_call_describe_volumes_on_ec2_client.png)

Step 5: Add for loop which will loop through all the volumes and create a snapshot on each volume this will mean you have to call the create snapshot function on the ec2 client in the for loop

[Loop through volumes and create snapshot for each volume](/images/06_add_for_loop_which_will_loop_through_all_volumes_and_create_a_snapshot_on_each_volume_this_will_mean_you_have_to_call_the_create_snapshot_function_on_the_ec2_client_in_the_for_loop.png)

[Snapshot created](/images/07_snapshot_created_on_aws.png)

Step 6: Import schedule library 

[Import schedule](/images/08_import_schedule_library.png)

Step 7: Because we are going to need a function to run timed schedules we will need to put the code for creating snapshot for every volume into a function 

Step 8:Add schedule function to create snapshot every 5 seconds to test if it works 

[Added schedule function](/images/09_add_schedule_functions_to_create_snapshot_every_5_seconds_to_test_if_it_works.png)

[Snapshot created on AWS](/images/10_snapshot_created_on_aws_due_to_scheduler.png)

Step 9: Create another python file and import boto3 library into it 

[Another python file](/images/11_create_new_python_file_and_import_boto3_library_into_it.png)

Step 10: Call ec2 client 

[ec2 client call](/images/12_call_ec2_client.png)

Step 11: Call describe snapshots function on ec2 client and insert parameter self this is because we only want snapshots that we created 

[Describe snapshots](/images/13__call_describe_snapshots_function_on_ec2_client_and_insert_parameter_self_this_is_because_we_only_want_snapshots_that_we_created.png)

Step 12: Print to check result 

[Checking Result](/images/14_print_to_check_result.png)

Step 13: Import itemgetter function from module operator

[import itemgetter](/images/15_import_itemgetter_function_from_module_operator.png)

Step 14: Use sorted to sort snapshot with the help of itemgetter function

[sorted](/images/16_use_sorted_to_sort_snapshots_with_the_help_of_itemgetter_function.png)

Step 15: Add for loop that will itterate through the sorted list of snapshot ids and delete every snapshot apart from the first snapshot that appears on the list which is the most recent snapshot

[loop to delete old snapshots](/images/17_add_for_loop_that_will-tterate_through_the_sorted_list_of_snapshot_ids_and_delete_every_snapshot_apart_from_the_first_snapshot_that_appears_on_the_list_which_is_the_most_recent_snapshot.png)

Step 16: Run the program 

[Run program](/images/18_run_the_program.png)

[snapshot on AWS](/images/19_snapshot_on_aws.png)

Step 17: Create new python file for new program that will restore volumes

[Python file for restoring volume](/images/20_create_new_python_file_for_new_proggram_that_will_restore_volume.png)

Step 18: Import boto3 library and create ec2 client and resource 

[ec2 client and resource](/images/21_import_boto3_library_and_create_ec2_client_and_resource.png)

Step 19: hardcode instance id that you want to restore volume and attach it to a variable

[Hardcode instance id](/images/22_hardcode_instance_id_that_you_want_to_resotre_volume_and_attach_it_to_a_variable%20.png)

Step 20: Use function describe volume to get volume of instance and add filter that takes instance id

[20 Describe volume](/images/23_use_function_describe_volume_to_get_volume_of_instance_add_a_filter_that_that_takes_instance_id.png)

Step 21: Grab the first object of the volume list and save this into a variable

[Grab first object](/images/24_grab_the_first_object_of_the_volumes_list_save_this_into_a_variable%20.png)

Step 22: Print to see volume object

[Volume object](/images/25_print_to_see_volume_object.png)

Step 23: Call function describe snapshots on ec2 client and add filter in paramater to get snapshots specifically for a volume using the volume id 

[Describe snapshot](/images/26_call_function_describe_snapshots_on_ec2_client_add_filter_in_param_to_get_snapshots_specifically_for_a_volume_using_the_volume_id.png)

Step 24: Import itemgetter function from operator module

[itemgetter](/images/27_import_item_getter_function_from_operator_module.png)

Step 25: Use sorted to sort snapshots list from most recent using the help of itemgetter

[Sort snapshot list](/images/28_use_sorted_to_sort_snapshots_list_from_most_recent_using_the_help_of_itemgetter.png)

Step 26: Grab the first object in the list which is the latest snapshot with index 0

[index 0](/images/29_grab_the_first_object_in_the_list_which_is_the_latest_snapshot_with_index_0.png)

Step 27: Use function create new volume on the ec2 client and insert snapshotid you want volume to be created with availability zone and tag

[Create volume](/images/30_use_function_create_new_volume_on_the_ec2_client_and_insert_snapshotid_you_want_volume_to_be_created_with_avail_zone_and_tag.png)

Step 28: Use instance function with instance id as parameter on ec2 resource and use attach volume function to attach created volume to the instance 

[Instance function](/images/31_use_instance_function_with_instance_id_as_parameter_on_ec2_resource_and_use_attach_volume_functin_to_attach_created_volume_to_the_instance.png)

Step 29: Add while loop to keep looping through up until the volume state is available this will mean we will need to add an if statement that will check its state 

[While loop](/images/32_add_while_loop_to_keep_looping_through_up_until_the_volume_state_is_available_this_will_mean_we_will_need_to_add_an_if_statement_that_will_check_its_state.png)

Step 30: Run program

[Run Program](/images/33_run_program.png)

[Volume on AWS](/images/34_volume_on_aws.png)

[Volume attached to instance](/images/35_volume_attached_to_instance.png)


## Installation

    brew install python3


## How to Contribute

1. Clone the repo using $ git clone git@gitlab.com:omacodes98/python-data-backup-and-restore.git

2. Create a new branch $ git checkout -b your name 

3. Make Changes and test 

4. Submit a pull request with description for review



## Questions

Feel free to contact me for further questions via: 

Gitlab: https://gitlab.com/omacodes98

Email: omacodes98@gmail.com

## References

https://gitlab.com/omacodes98/python-data-backup-and-restore

## License

The MIT License 

For more informaation you can click the link below:

https://opensource.org/licenses/MIT

Copyright (c) 2022 Omatsola Beji.